/*@author: Jacek Skowron
 * The script creates new bug and sets most of fields to test values.
 * Before running the test set unique 'bugSubject' 
 * it'll be used in the scripts: 'BugUpdate.java' and 'BugSearch.java'.*/

package sel_2;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;

import org.junit.*;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.Random;

public class NewBug {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();  
  protected static String bugSubject = "test_subject_16";
  
  /*int randomInt = getRand();
  
  public int getRand(){
	  Random rand = new Random();
	  int randomInt = rand.nextInt(100);	
	  return randomInt;
  }

  String bugSubject = getSubject("test_subject_");
  
  public String getSubject(String s){
	  s += randomInt;
	  return s;
  }*/
    
  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://jira.atlassian.com/browse/TST/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testNewBug() throws Exception {
	//logging into JIRA 
    driver.get(baseUrl);
    driver.findElement(By.linkText("Log In")).click();
    driver.findElement(By.id("username")).clear();
    driver.findElement(By.id("username")).sendKeys("jacek.skowron6@gmail.com");
    driver.findElement(By.id("password")).clear();
    Thread.sleep(2000);
    driver.findElement(By.id("password")).sendKeys("testtesttest");
    driver.findElement(By.id("login-submit")).click();
    Thread.sleep(5000);
    //filling fields with test data
    driver.findElement(By.id("create_link")).click();
    driver.findElement(By.xpath("//div[@id='issuetype-single-select']/span")).click();
    driver.findElement(By.linkText("Bug")).click();
    Thread.sleep(5000);
    driver.findElement(By.id("summary")).click();
    driver.findElement(By.id("summary")).clear();
    driver.findElement(By.id("summary")).sendKeys(bugSubject);
    Thread.sleep(3000);
    driver.findElement(By.xpath("(//span[@class='icon aui-ss-icon noloading drop-menu'])[3]")).click();
    Thread.sleep(3000);
    driver.findElement(By.xpath("//div[@id='priority-suggestions']/div/ul/li[2]")).click();
    driver.findElement(By.id("duedate")).sendKeys("25/Nov/14");
    driver.findElement(By.xpath("//div[@id='components-multi-select']/span")).click();
    driver.findElement(By.linkText("Component 1")).click();
    driver.findElement(By.xpath("//div[@id='versions-multi-select']/span")).click();
    driver.findElement(By.xpath("(//a[contains(text(),'Version 2.0')])[2]")).click();
    driver.findElement(By.xpath("//div[@id='fixVersions-multi-select']/span")).click();
    driver.findElement(By.xpath("(//a[contains(text(),'Version 2.0')])[3]")).click();
    driver.findElement(By.xpath("//div[@id='assignee-single-select']/span")).click();
    driver.findElement(By.id("assignee-field")).clear();
    driver.findElement(By.id("assignee-field")).sendKeys("Unassigned");
    driver.findElement(By.id("environment")).clear();
    driver.findElement(By.id("environment")).sendKeys("Windows 7");
    driver.findElement(By.id("description")).clear();
    driver.findElement(By.id("description")).sendKeys("Description: test text\nSteps:\n1. test text\n2. test text\n3. test text\n\nExpected result: test text\nActual result: test text");
    driver.findElement(By.id("customfield_10041")).sendKeys("25/Nov/14");
    driver.findElement(By.id("customfield_10064-1")).click();
    driver.findElement(By.id("timetracking")).clear();
    driver.findElement(By.id("timetracking")).sendKeys("3w 4d 12d");
    new Select(driver.findElement(By.id("customfield_10550"))).selectByVisibleText("Version 2.0");
    driver.findElement(By.xpath("//div[@id='labels-multi-select']/span")).click();
    driver.findElement(By.linkText("blue")).click();
    Thread.sleep(3000);
    driver.findElement(By.xpath("//div[@id='customfield_11930-single-select']/span")).click();
    driver.findElement(By.id("customfield_10653")).clear();
    driver.findElement(By.id("customfield_10653")).sendKeys("10");
    Thread.sleep(3000);
    driver.findElement(By.id("customfield_11930-field")).click();
    //driver.findElement(By.linkText("35mm Capture - 2.7.1 (Future sprint)")).click();
    driver.findElement(By.xpath("//a[@title='35mm Capture - 2.7.1']")).click();
    driver.findElement(By.xpath("//div[@id='customfield_12931-single-select']/span")).click();
    driver.findElement(By.linkText("unlabelled-FE-4117 - (FE-4117)")).click();
    Thread.sleep(3000);
    driver.findElement(By.id("create-issue-submit")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
