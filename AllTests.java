package sel_2;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ NewBug.class, BugUpdate.class, BugSearch.class })
public class AllTests {

}
