/*@author: Jacek Skowron
 * The test checks if the bug created in the script 'NewBug.java' can be updated. 
 * The update entails bug name and environment change.
 * After change updated bug is searched and checked if it appears in <h1> node.
 * Change bugSubject value to update the desired bug.*/

package sel_2;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;

import org.junit.*;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.Keys;

public class BugUpdate{
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();
  //String bugSubject = "test_subject_14";
  String bugSubjectUpdated = getUpdatedSubject(NewBug.bugSubject);
  
  String getUpdatedSubject(String bS){
	  return bS + "_update";
  }
  
  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://jira.atlassian.com/browse/TST/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testBugUpdate() throws Exception {
	//logging into JIRA 
    driver.get(baseUrl);
    driver.findElement(By.linkText("Log In")).click();
    driver.findElement(By.id("username")).clear();
    driver.findElement(By.id("username")).sendKeys("jacek.skowron6@gmail.com");
    driver.findElement(By.id("password")).clear();
    Thread.sleep(2000);
    driver.findElement(By.id("password")).sendKeys("testtesttest");
    driver.findElement(By.id("login-submit")).click();
    Thread.sleep(4000);
    driver.findElement(By.id("quickSearchInput")).click();
    Thread.sleep(4000);
    //click ENTER while empty search box active to be able to use search options
    driver.findElement(By.id("quickSearchInput")).sendKeys(NewBug.bugSubject);
    Thread.sleep(4000);
    driver.findElement(By.id("quickSearchInput")).sendKeys(Keys.ENTER);
    Thread.sleep(5000);
    driver.findElement(By.xpath("//ul[@id='opsbar-edit-issue_container']")).click();
    Thread.sleep(5000);
    //updating fields in JIRA form
    driver.findElement(By.id("summary")).clear();
    driver.findElement(By.id("summary")).sendKeys(bugSubjectUpdated);
    driver.findElement(By.id("environment")).clear();
    driver.findElement(By.id("environment")).sendKeys("Windows 8");
    driver.findElement(By.id("edit-issue-submit")).click();
    driver.findElement(By.xpath("//body[@id='jira']/div[9]/div")).click();
    //looking up 
    driver.findElement(By.id("quickSearchInput")).sendKeys(bugSubjectUpdated);
    Thread.sleep(4000);
    driver.findElement(By.id("quickSearchInput")).sendKeys(Keys.ENTER);
    Thread.sleep(5000);
    driver.findElement(By.xpath("//h1[@id='summary-val' and text()='" + bugSubjectUpdated + "']"));
    Thread.sleep(6000);
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
